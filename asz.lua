print "LOADED!"

print "  asz - show termo!";
print "  wifiUpdate - create wifi with value";
print "  wifize - start cyclic check";

iterationCount = 0
netFailCount = 0

function initBiRAP ()
   wifi.setmode(wifi.STATIONAP)
   cfg = {}
   cfg.ssid = "ℹ BiR infohub"
   wifi.ap.config(cfg)
end


function asz ()
  d = require('dht')
  t = require('ds18b20')
  t.setup(6);
  addrs = t.addrs()
  avgValue = 0;
  avgItems = 0;
  values = {};
--  print "Readling data..."
  for i = 1, #addrs do
--   print "Iterating address"
   sensorAddress = ''
   for j = 1, string.len(addrs[i])-1 do
     sensorAddress = sensorAddress .. string.format("%02x:",string.byte(addrs[i],j));
   end
   sensorAddress = sensorAddress .. string.format("%02x",string.byte(addrs[i],8));
--    uart.write(0, sensorAddress)
   t.read(addrs[i], t.C)
   value = t.read(addrs[i], t.C)
--   print('   ---> val: ', value);
   measurement = {sensor=sensorAddress, value=value}
   values[i] = measurement
--    print("  ",value)

   if (value == nil) then
      value = 100000
   end
   avgValue = avgValue + value
   avgItems = avgItems + 1
  end

  dht_return = {}
  dht_status, dht_temp, dht_humi = d.read(1)
  if (dht_status == dht.OK) then
   dht_return = {temp = dht_temp, humidity = dht_humi}
  end

  avg = string.format("%.2f",avgValue / avgItems)
--  print("   AVG: ", avg)
--  print("Values: ", values)
  return avg, values, dht_return;
end


function widePublish(values, avg, dht_return)

--   print "Publishing"
   http.post('http://aszu.pl:8181/dashboard/temperature', "Content-Type: application/json\r\n",
   cjson.encode({time=rtctime.get()*1000, data=values, avg=avg, iteration = iterationCount, heap = node.heap(), dht = dht_return}),
   function(code,data)
--      print("Pub result", code)
     if (code ~= 200 or rtctime.get()<1000000 ) then
       netFailCount = netFailCount + 1
     else
       stat = cjson.decode(data)
       if (stat.fan.state == 1) then
          gpio.write(5, gpio.HIGH);
       else
          gpio.write(5, gpio.LOW);
       end

       if (stat.leds.ledCount > 0) then
         ws2812.init();
         ledString = '';
         for j = 1, #stat.leds.ledValues do
            ledString = ledString .. string.char(stat.leds.ledValues[j][2],stat.leds.ledValues[j][1],stat.leds.ledValues[j][3]);
         end
         ws2812.write(ledString);
       end
     end
   end);
end


function wifiUpdate()
   avg, values, dht_return = asz()
   iterationCount = iterationCount + 1
   widePublish(values,avg,dht_return);
   if (netFailCount > 30) then
      node.restart()
   end
end


function wifize()
  require('tmr');
  tmr.alarm(0, 10000, tmr.ALARM_AUTO, wifiUpdate)
  tobj = tmr
end

initBiRAP ()
