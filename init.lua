function dynamicWifiConnect()
   print('[BOOT] dynamic wifi connect')
   wifi.setmode(wifi.STATIONAP)
   wifi.sta.getap( function (names)
      print('[BOOT] dynamic wifi connect scan completed')
      local gotWifi = 0
      local name
      for name in pairs(names) do
         print('[BOOT] wifi found: '..name)
         name = name:gsub(' ', '_')
         local result, existence = pcall(function() return file.exists('wifiauto_'..name..'.lua') end);
         if result and existence then
            gotWifi = 1
            print('[BOOT] got network config.... connecting')
            require('wifiauto_'..name)
            break
         end
      end
      if (gotWifi == 0) then
         print('[BOOT] Wifi not connected - no autofile.')
      end
   end)
end

function gotNetworkAction(prevState)
   print('[BOOT] Got wifi!')
   rtctime = require('rtctime')
   http.get('http://aszu.pl:8181/.../services/now/1000', nil,
      function(s,b)
         if tonumber(b) == nil or tonumber(b) < 10 then
            print('[BOOT] Cannot feed RTC with "'..b..'"')
            node.restart()
         else
            rtctime.set(tonumber(b))
            print('[BOOT] RTC set, '..rtctime.get())
            pcall(function() require('updateAgent'):init() end)
            pcall(startApplication)
         end
      end
   )
end

function initAction()
   print('[BOOT] initAction... Checking for MAC.')
   if (file.exists('mac.cfg')) then
      file.open('mac.cfg');
      wifi.sta.setmac(file.read(17));
      file.close()
   else
      print('[BOOT] initAction... No MAC override.')
   end

   -- connect to wifi, prepare to set current time on connection made.

   wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, gotNetworkAction)

   --wifi.sta.eventMonStart()

   --wifi.sta.connect()
   pcall(dynamicWifiConnect)

end


function startApplication()
   if (file.exists('autostart.lua')) then
      print('[BOOT] autostart.lua found - executing.')
      require('autostart');
   else
      print('[BOOT] autostart.lua not found, do something manually.')
   end
end

bootstrapAlarm = tmr.create()
bootstrapAlarm:register(5000,tmr.ALARM_SINGLE,
function ()
   node.setcpufreq(node.CPU160MHZ)
   print('[BOOT] Initialization...')
   if (file.exists('certificate.lua')) then
      print("[BOOT] certificate.lua found, updating certificate");
      require('certificate')
      file.remove('certificate.lua')
      node.restart()
   else
      print("[BOOT] certificate.lua not found");
   end
   if (file.exists('preinit.lua')) then
      require('preinit')
   else
      print("[BOOT] preinit.lua not found");
   end
   pcall(initAction)
end
);
bootstrapAlarm:start()
