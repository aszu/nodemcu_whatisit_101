local updateAgent = {}


function updateAgent:init(checkDelay)
   if (checkDelay == nil) then
      checkDelay = 900
   end
   self.config = {
      host = 'aszu.pl',
      port = 1082,
      product = 'nodemcu-core'
   }
   if (file.exists('update.cfg')) then
      if (file.open('update.cfg')) then
         self.config = cjson.decode(file.read())
         file.close()
      else
         print('[UPDATE] updateAgent cannot read config')
         return;
      end
   end
   self.tmr = tmr.create()
   self.tmr:register(checkDelay * 1000, tmr.ALARM_AUTO, function () self:callback() end )
   self.tmr:start()
end

function updateAgent:callback()
   self:checkUpdate()
end

function updateAgent:checkUpdate()
   if (file.exists('versions.json')) then
      if (file.open('versions.json')) then
         self.versions = cjson.decode(file.read())
         file.close()
      else
         self.versions = {}
      end
   else
      self.versions = {}
   end

   self.state = 'STARTING'
   self.feature = 'VERSIONS'
   self.availVersions = {}
   self.updatelist = {}
   self.tmp = {}
   local conn = net.createConnection(net.TCP, 0)
   conn:on('connection', function (sock) self:connected(sock) end )
   conn:on('receive', function(sock, data) self:dataReceived(sock, data) end)
   conn:on('disconnection', function(sock) self:transferCompleted(sock) end)
   conn:connect(self.config.port, self.config.host)

end

function updateAgent:connected(socket)
   self.state = 'SWITCHING';
   socket:send("update\n");
end

function updateAgent:dataReceived(socket, data)
   if (self.state == 'SWITCHING') then
      if (data == 'PASSING\n') then
         if (self.feature == 'VERSIONS') then
            print('[UPDATE] Checking available versions')
            socket:send('SWVERSION:'..self.config.product..'\n')
            self.state = 'LISTENING_VERSIONS'
         else
            if (self.feature == 'FILE') then
               socket:send('SWDOWNLOAD:'..self.config.product..':'..self.tmp.file..':'..self.tmp.version)
               self.state = 'DOWNLOADING_FILE'
            end
         end
      else
         socket:close()
      end
   else
      if (self.state == 'LISTENING_VERSIONS') then
         if (self.data == nil) then
            self.data = ''
         end
         self.data = self.data .. data
         updateAgent:processLines()
      end
      if (self.state == 'DOWNLOADING_FILE') then
         file.write(data)
      end
   end
end

function updateAgent:processLines()
   if (self.state == 'LISTENING_VERSIONS') then
      local line = 'X'
      while (line ~= nil) do
         line, self.data = self:readLineFromString(self.data)
         if (line ~= nil) then
            if (string.sub(line, 1, 1) == '+') then

            else
               if (string.sub(line, 1, 1) == '-') then
                  local pos = string.find(line, ':')
                  if (self.availVersions[self.tmp.file] == nil) then
                     self.availVersions[self.tmp.file] = {}
                  end
                  self.availVersions[self.tmp.file][string.sub(line, 2, pos-1)] = string.sub(line, pos+1)
               else
                  if (string.sub(line, 1, 1) == '*') then
                     self.tmp.file = string.sub(line, 2)
                  end
               end
            end
         end
      end
   end
end

function updateAgent:transferCompleted(sock)
   if (self.state == 'LISTENING_VERSIONS') then
      self:scheduleDownloads()
   end
   if (self.state == 'DOWNLOADING_FILE') then
      file.close()
      file.remove(self.tmp.file)
      file.rename('update.tmp', self.tmp.file)
      self.versions[self.tmp.file] = self.tmp.version


      if (file.open('versions.json', 'w+')) then
         file.write(cjson.encode(self.versions))
         file.close()
      end

      self:startDownloads()

   end
   self.tmp = nil
   self.data = nil
end

function updateAgent:scheduleDownloads()
   local file,ver

   local updateToBePerformed = 0

   for file, ver in pairs(self.availVersions) do
      if (self.versions[file] == nil or self.versions[file] ~= ver['LATEST']) then
         print("[UPDATE] > updating "..file..' to version '..ver['LATEST'])
         table.insert(self.updatelist, {file = file, version = ver['LATEST']})
         updateToBePerformed = 1
      end
   end

   if (updateToBePerformed == 1) then
      self:startDownloads()
   end

end

function updateAgent:startDownloads()
   local dlTimer
   dlTimer = tmr.create()
   dlTimer:register(1000, tmr.ALARM_SINGLE, function() self:startDownloadFile() end)
   dlTimer:start()
end

function updateAgent:startDownloadFile()
   local spec
   spec = table.remove(self.updatelist)
   if (spec ~= nil) then
      print("[UPDATE] Downloading "..spec.file..' version '..spec.version)

      if (file.open('update.tmp', 'w+')) then

         self.feature = 'FILE'
         self.state = 'STARTING'
         self.tmp = {file = spec.file, version = spec.version}
         local conn = net.createConnection(net.TCP, 0)
         conn:on('connection', function (sock) self:connected(sock) end )
         conn:on('receive', function(sock, data) self:dataReceived(sock, data) end)
         conn:on('disconnection', function(sock) self:transferCompleted(sock) end)
         conn:connect(self.config.port, self.config.host)

      else

         table.insert(self.updatelist, spec)
         self:startDownloads()

      end

   else
      node.restart()
   end

end

function updateAgent:fileBeingReceived(sock, data)
end

function updateAgent:readLineFromString(str)
   local pos = string.find(str, '\n')
   if (pos ~= nil) then
      local result = string.sub(str, 1, pos - 1)
      str = string.sub(str, pos+1)
      return result, str
   else
      return nil, str
   end
end


function ua()
   package.loaded['updateAgent'] = nil
   return require('updateAgent')
end

return updateAgent;